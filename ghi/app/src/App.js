import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList.js'
import ShoeForm from './ShoesForm.js';
import HatsForm from "./HatsForm";
import HatsList from "./HatsList";
import React from "react";

function App(props) {
  if(props.shoes === undefined && props.hats === undefined){
    return null
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='/shoes' element={<ShoesList shoes = {props.shoes} />} />
          <Route path='/shoes/new' element={<ShoeForm />} />
          <Route path="hats" element={<HatsList hats={props.hats} />} />
          <Route path="hats/new" element={<HatsForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
