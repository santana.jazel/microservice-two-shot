import React, { useEffect, useState } from "react";

function HatsForm() {
    const [locations, setLocations] = useState([]);
    const [formData, setFormData] = useState({
        fabric: "",
        style_name: "",
        color: "",
        picture_url: "",
        location: "",
    })

    // handle submit
    const handleSubmit = async (event) => {
        event.preventDefault();

        const hatData = { ...formData };
        // hatData.fabric = fabric;
        // hatData.style_name = style_name;
        // hatData.color = colorl
        // hatData.picture_url = picture_url;
        // hatData.location = location;

        const hatUrl = "http://localhost:8090/api/hats/"
        console.log(formData)
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(hatData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        try {
            const hatResponse = await fetch(hatUrl, fetchConfig)
            if (hatResponse.ok) {
                const newHat = await hatResponse.json();
                console.log(newHat)
                setFormData({
                    fabric: "",
                    style_name:"",
                    color: "",
                    picture_url: "",
                    location: "",
                })
            } else {
                throw new Error("wronggg failed creating hat")
            }
        } catch (error) {
            console.error("Error:", error);
        }
    }

    // onChange function
    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setFormData((PreviousData) => ({
            ...PreviousData,
            [name]: value
        }))
    }

    // list location functions
    const fetchData = async () => {
        const locationUrl = "http://localhost:8100/api/locations/"
        const response = await fetch(locationUrl);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new hat Location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input
                    onChange={handleInputChange}
                    value={formData.fabric}
                    placeholder="fabric"
                    required type="text"
                    name="fabric"
                    id="fabric"
                    className="form-control" />
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                    onChange={handleInputChange}
                    value={formData.style_name}
                    placeholder="style_name"
                    required type="text"
                    name="style_name"
                    id="style_name"
                    className="form-control" />
                    <label htmlFor="style_name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                    onChange={handleInputChange}
                    value={formData.color}
                    placeholder="color"
                    required type="text"
                    name="color"
                    id="color"
                    className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input
                    onChange={handleInputChange}
                    value={formData.picture_url}
                    placeholder="picture_url"
                    type="url"
                    name="picture_url"
                    id="color"
                    className="form-control" />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select
                    onChange={handleInputChange}
                    value={formData.location}
                    required
                    name="location"
                    id="location"
                    className="form-select">
                <option value="">Choose a Location</option>
                {locations.map((location) => (
                    <option value={location.href} key={location.href}>
                    {location.closet_name}
                </option>
                ))}
                    </select>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
        </form>
        </div>
    </div>
    </div>
    );
};

export default HatsForm;
