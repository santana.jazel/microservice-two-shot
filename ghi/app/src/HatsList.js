import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function HatsList(props) {
    const {hats} = props

    const[items, setItems] = React.useState(props.hats);
    const [deletionMessage, setDeletionMessage] = useState("");

    const deleteItem = async (id) => {
        const url = `http://localhost:8090/api/hats/${id}/`;
        const fetchConfig = {
            method: "delete",
        };

        try {
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const deleted = await response.json();
            console.log(deleted)
            window.location.reload()
            setDeletionMessage("Item deleted successfully")
        } else {
            throw new Error("Deletion failed");
            }
        } catch (error) {
            console.error("Error:", error);
            setDeletionMessage("failed to deleted item")
        }


    }

    return (
    <>
        {deletionMessage && (<p className="alert alert-success">{deletionMessage}</p>)}
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>StyleName</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
            {hats.map((hat) =>  {
                return(
                    <tr key={hat.id}>
                        <td>{hat.fabric}</td>
                        <td>{hat.style_name}</td>
                        <td>{hat.color}</td>
                        <td>{hat.location.closet_name}</td>
                        <td>
                            <img src={hat.picture_url} width="100" />
                        </td>
                        <td>
                            <button
                            className="btn btn-danger"
                            onClick={() => deleteItem(hat.id)}>
                                Delete
                            </button>
                        </td>
                    </tr>
                    )
                }
            )}
            </tbody>
        </table>
            <div className="link">
                <Link className="link" to="/hats/new">
                    Create a new hat
                </Link>
            </div>
    </>
    )
}

export default HatsList;
