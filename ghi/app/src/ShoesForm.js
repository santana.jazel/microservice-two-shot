import React, {useEffect, useState} from 'react'

export default function ShoeForm(){

    const [bins, setBins] = useState([])

    const [formData, setFormData] = useState({
        manufacturer_name: '',
        model_name: '',
        color: '',
        image_url: '',
        bin: ''
    })

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url)

        if(response.ok){
            const data = await response.json()
            setBins(data.bins)
        }else{
            console.error('Failure fetching bins')
        }
    }
    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (e) => {
        e.preventDefault()

        // const url = `http://localhost:8080/api/bins/${formData.bin}/shoes/`
        const url = `http://localhost:8080/api/shoes/`
        console.log('formdata.bin:', formData.bin)
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        console.log('submitting form data:', formData)
        const response = await fetch(url, fetchConfig)
        console.log('response:', response)

        try{
            if(response.ok){
                response.json().then(() => {
                    setFormData({
                        manufacturer_name: '',
                        model_name: '',
                        color: '',
                        image_url: '',
                        bin: ''
                    })
                })
            }
        }catch(e){
            console.error('Failed to submit data', e)
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name

        console.log(`Changing ${inputName} to ${value}`)

        setFormData({
            ...formData,
            [inputName]: inputName === 'bin' ? bins.find((bin) =>
            bin.closet_name === value).href : value
        })
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add new shoes</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Manufacturer Name" required type="text" value={formData.manufacturer_name} name="manufacturer_name" id="manufacturer_name" className="form-control"/>
                                <label htmlFor="manufacturer_name">Manufacturer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Model Name" required type="text" name="model_name" value={formData.model_name} id="model_name" className="form-control"/>
                                <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Color" type="text" name="color" id="color" value={formData.color} className="form-control"/>
                                <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Image Url" type="text" name="image_url" id="image_url"  value={formData.image_url} className="form-control"/>
                                <label htmlFor="image_url">Image Url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="bin" value={formData.bin.closet_name} id="bin" className="form-select">
                                <option value="">Choose a bin</option>
                                {bins.map( (bin) =>{
                                    let binId = bin.import_href ? bin.import_href.split('/').filter(Number).pop(): null
                                    return (
                                        <option key={bin.id} value={binId}>{bin.closet_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
