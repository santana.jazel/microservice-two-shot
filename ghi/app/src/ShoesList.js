import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'


export default function ShoesList(props) {
    const { shoes } = props
    const [deleteMessage, setDeleteMessage] = useState('')

    const deleteShoe = async (id) => {
        try{
            const url = `http://localhost:8080/api/shoes/${id}`
            const response = await fetch(url, {
                method: 'DELETE'
            })

            if (response.ok){
                const deletedShoe = await response.json()
                setDeleteMessage('Item deleted successfully')
                window.location.reload()
            }
        }catch(e){
            console.error('Failed to delete shoe', e)
        }
    }

    return (
            <>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Color</th>
                        <th>Bin</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map((shoe) => {
                        return (
                            <tr key={shoe.id}>
                                <td>{shoe.manufacturer_name}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.bin.closet_name}</td>
                                <td>
                                    <img src={shoe.image_url} style={{ width: '100px', height: '100px' }} alt='shoe' />
                                </td>
                                <td>
                                    <button onClick={() => deleteShoe(shoe.id)} className='btn btn-danger'>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <div className="navbar navbar-light bg-light">
                <Link className="form-inline" to='/shoes/new'>
                    <button className="btn btn-outline-success" type="button">Add Shoes</button>
                </Link>
            </div>
        </>
    )
}
