import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css'
// import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadData() {
  try {
    const response = await fetch('http://localhost:8080/api/shoes')
    const hatsResponse = await fetch("http://localhost:8090/api/hats/");
    if(response.ok && hatsResponse.ok){
      const data = await response.json()
      const hatData = await hatsResponse.json()
      console.log(data)

      root.render(
        <React.StrictMode>
          <App shoes={data.shoes} hats={hatData.hats} />
        </React.StrictMode>
      )
    }else{
      console.error('failed to load data list', response.status, response.statusText)
    }
  }catch(e){
    console.log('error fetching data', e)
  }
}

loadData()

// reportWebVitals();
