from django.contrib import admin
from .models import Hat, LocationVO

# Register your models here.
@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    pass
    # list_display = (
    #     "style_name",
    #     "color",
    #     "picture_url"
    # )

@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    pass
    # list_display = (
    #     "closet_name",
    #     "section_number",
    #     "shelf_number",
    # )
