from django.db import models


# Create your models here.

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

    # def __str__(self):
    #     return f'{self.bin.closet_name} - {self.bin.bin_number}/{self.bin.bin_size}'


class Shoes(models.Model):
    manufacturer_name = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    image_url = models.CharField(max_length=200, null=True, blank=True)

    bin = models.ForeignKey(
        BinVO,
        related_name='shoes',
        on_delete=models.CASCADE

    )
