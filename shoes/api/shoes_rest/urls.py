from django.urls import path
from .views import list_shoes, show_shoe_details

urlpatterns = [
    path('shoes/', list_shoes, name='create_shoes'),
    path('bin/<int:bin_vo_id>/shoes/', list_shoes, name='list_shoes'),
    path('shoes/<int:id>/', show_shoe_details, name='show_shoe_details')
]
