from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Shoes, BinVO

# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ['closet_name', 'import_href']

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ['model_name', 'manufacturer_name', 'bin', 'id' ]

    encoders = {'bin': BinVODetailEncoder()}

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ['model_name', 'manufacturer_name', 'color', 'image_url', 'bin', 'id']

    encoders = {'bin': BinVODetailEncoder()}

    # def get_extra_data(self, o):
    #     bin = BinVO.objects.filter(import_href=o.import_href)
    #     return bin

@require_http_methods(['GET', 'POST'])
def list_shoes(request,bin_vo_id=None):

    if request.method == 'GET':
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse({'shoes': shoes}, encoder=ShoesDetailEncoder)
    else:
        content = json.loads(request.body)

        try:
            bin_href = content['bin']
            bin = BinVO.objects.get(import_href=bin_href)
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({'error': 'Bin ID is not valid'}, status=400)

        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False
        )

@require_http_methods(['GET', 'DELETE',])
def show_shoe_details(request, id):

    if request.method == 'GET':
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False
        )
    else:
        # if request.method == 'DELETE':
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})
    # else:
    #     content = json.loads(request.body)
    #     try:
    #         shoe = Shoes.objects.get
    #     except
